//
//  AppDelegate.swift
//  dareyou
//
//  Created by Manuel on 01/02/2020.
//  Copyright © 2020 Manuel. All rights reserved.
//

import Cocoa

let logLevel: LogLevel = .info

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    var window: NSWindow?
    var app: ((Message) -> ())!

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        let ui = UI{ self.app($0) }
        window = ui.window()
        
        let receivers = [ui.handle(msg: )]
        app = createApp(rootHandler: {
            self.app($0)
        }, logLevel: logLevel, receivers: receivers)
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

