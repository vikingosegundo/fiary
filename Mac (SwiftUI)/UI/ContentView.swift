//
//  ContentView.swift
//  dareyou
//
//  Created by Manuel on 01/02/2020.
//  Copyright © 2020 Manuel. All rights reserved.
//

import Combine
import SwiftUI

fileprivate
class StateHolder: ObservableObject {
    let objectWillChange = ObservableObjectPublisher()
    var random: Int? { willSet {
        objectWillChange.send() } }
    var averageOf4: Double?{ willSet { objectWillChange.send() } }
    var averageOf5: Double?{ willSet { objectWillChange.send() } }
    
}

struct ContentView: View {
    
    init(handler: @escaping (Message) -> ()) {
        self.handler = handler
    }
    
    @ObservedObject fileprivate var state = StateHolder()
    
    private let handler: (Message) -> ()
    
    var body: some View {
        let rdm = state.random  != nil ? "\(state.random!)" : "--"
        let av4 = state.averageOf4 != nil ? "\(state.averageOf4!)" : "--"
        let av5 = state.averageOf5 != nil ? "\(state.averageOf5!)" : "--"
        
        return VStack {
            HStack {
                Text("\(rdm)").frame(width: 45, height:30, alignment: .center)
                Text("\(av4)").frame(width: 45, height:30, alignment: .center)
                Text("\(av5)").frame(width: 45, height:30, alignment: .center)
            }.padding(30)
            Button(action: {
                self.handler(.feature(.random(.range(0, 100))))
            }
            ) {
                Text("Button")
            }.frame(width: nil, height: nil, alignment: .center).padding()
        }
    }
    
    func handle(msg: Message) {
        switch msg {
        case .feature(.random(.pickedRandomly(let x))):
            if let x = x as? Int{
                state.random = x
            }
            
        case .feature(.random(.averageFromLastPicks(let picks, let value))):
            switch picks {
            case 4:
                state.averageOf4 = value
            case 5:
                state.averageOf5 = value
            default:
                break
            }
        default:
            break
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(handler: { _ in })
    }
}
