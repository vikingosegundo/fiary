//
//  UI.swift
//  dareyou
//
//  Created by Manuel on 01/02/2020.
//  Copyright © 2020 Manuel. All rights reserved.
//

import Foundation
import SwiftUI

class UI {
    
    init(rootHandler: @escaping (Message) -> ()) {
        self.rootHandler = rootHandler
        contentView = ContentView(handler: rootHandler)
    }
    
    private let rootHandler: (Message) -> ()
    
    var contentView: ContentView?
    
    func window() -> NSWindow {
        // Create the window and set the content view.
        let window = NSWindow(
            contentRect: NSRect(x: 0, y: 0, width: 480, height: 300),
            styleMask: [.titled, .closable, .miniaturizable, .resizable, .fullSizeContentView],
            backing: .buffered, defer: false)
        window.center()
        window.setFrameAutosaveName("Main Window")
        window.contentView = NSHostingView(rootView: contentView)
        window.makeKeyAndOrderFront(nil)
        return window
    }

    func handle(msg: Message) {
        contentView?.handle(msg: msg)
    }
}

