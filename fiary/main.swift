//
//  main.swift
//  fiary
//
//  Created by Manuel Meyer on 15/03/2020.
//  Copyright © 2020 Manuel. All rights reserved.
//

var callCounter = 0
var diceStrings: [String] = []

func printer(_ msg:Message) -> () {
    if case .feature(.random(.pickedRandomly(let x))) = msg {
        print("\(diceStrings[callCounter]) \t -> \(x)")
        callCounter = callCounter + 1
    }
}

var app: ((Message) -> ())!
app = createApp(rootHandler: { app($0) },
                   logLevel: .none,
                  receivers: [printer]
)

for arg in Array(CommandLine.arguments.dropFirst()) {
    let a = arg.split(separator: "x")
    guard
        a.count == 2,
        let numberOfRolls = Int(a[0]),
        let upperbound    = Int(a[1])
    else { continue }
    
    for _ in 0..<numberOfRolls {
        diceStrings.append(arg)
        app(.feature(.random(.range(1, upperbound + 1))))
    }
}

