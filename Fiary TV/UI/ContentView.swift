//
//  ContentView.swift
//  dareyou
//
//  Created by Manuel on 01/02/2020.
//  Copyright © 2020 Manuel. All rights reserved.
//

import Combine
import SwiftUI

fileprivate
func createDieView(die: Die, value: Int?) -> some View {
    guard let value = value else { return AnyView(Text(" ").font(.largeTitle))}
    switch die.faceStyle {
    case .text(let p): return AnyView(p(value).font(.largeTitle))
    case .cube(let p): return AnyView(p(value).font(.system(size: 256)))
    case .image(let p): return AnyView(p(value))
    }
}

fileprivate let dice = [
    Die(title: "1-6",  range: 1...6,  faceStyle: .cube ({ Text(cubeFaces[$0 - 1]) })),
    Die(title: "Coin", range: 0...1,  faceStyle: .image({ coinFaces[$0] })),
    Die(title: "0-99", range: 0...99, faceStyle: .text ({ Text("\($0)") })),
    Die(title: "1-20", range: 1...20, faceStyle: .text ({ Text("\($0)") }))
]


fileprivate
class StateHolder: ObservableObject {
    let objectWillChange = ObservableObjectPublisher()
    var random: Int? { willSet { objectWillChange.send() } }
    var die: Die = dice[1] { willSet { random = nil; objectWillChange.send() } }
}

struct ContentView: View {
    
    init(handler: @escaping (Message) -> ()) {
        self.handler = handler
        self.state = StateHolder()
    }
    
    @ObservedObject fileprivate var state: StateHolder
    @State private var dieIsRolling = false
    
    private let handler: (Message) -> ()
    
    private func rollDie() {
        self.dieIsRolling = true
        self.state.random = nil
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
            self.handler(.feature(.random(.range(
                self.state.die.range.lowerBound,
                self.state.die.range.upperBound + 1)
                )))
            self.dieIsRolling = false
        }
    }
    
    var body: some View {
        VStack(alignment:.center) {
            Picker(selection: $state.die, label: Text("Choose your dice")) {
                ForEach(dice, id: \.self) { Text($0.title) }
            }.pickerStyle(SegmentedPickerStyle())
            Divider()
            Spacer()
            createDieView(
                die: state.die,
                value: state.random
            ).frame(width: nil, height: 400, alignment: .center)
            Spacer()
            Button(action: self.rollDie){ Text("Roll Dice") }.disabled(dieIsRolling)
        }
    }
    
    func handle(msg: Message) {
        if
            case .feature(.random(.pickedRandomly(let x))) = msg,
            let xInt = x as? Int
        {
            state.random = xInt
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(handler: { _ in })
    }
}



fileprivate let cubeFaces = ["⚀", "⚁","⚂","⚃","⚄","⚅"]
fileprivate let coinHeads = Image(systemName: "star.fill")
fileprivate let coinTails = Image(systemName: "star")
fileprivate let coinFaces = [coinHeads, coinTails]
