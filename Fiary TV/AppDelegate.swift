//
//  AppDelegate.swift
//  Fiary TV
//
//  Created by Manuel on 02/02/2020.
//  Copyright © 2020 Manuel. All rights reserved.
//

import UIKit

let logLevel: LogLevel = .info

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var app: ((Message) -> ())!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let ui = UI {
            self.app($0)
        }
        window = ui.window()
        
        let receivers = [ui.handle(msg: )]
        app = createApp(rootHandler: {
            self.app($0)
        }, logLevel: logLevel, receivers: receivers)
        
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        app(.log(.info(LogEntry(location: NSStringFromClass(type(of: self)), explanation: "did become active", message: nil))))
        app(.state)
    }
}

