//
//  createApp.swift
//  dareyou
//
//  Created by Manuel on 01/02/2020.
//  Copyright © 2020 Manuel. All rights reserved.
//

func createApp(rootHandler: @escaping (Message) -> (),
                  logLevel: LogLevel,
                 receivers: [(Message) -> ()] ) -> (Message) -> ()
{
    let logger: (String) -> () = { print("\($0)") }

    let features = [
        log(with: logger, level: logLevel),
        random(handler: rootHandler),
        average(ofLast: 3, handler: rootHandler),
        average(ofLast: 4, handler: rootHandler)
    ]
    return { msg in (receivers + features).forEach { $0(msg) } }
}
