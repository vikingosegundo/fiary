//
//  logging.swift
//  Fiary
//
//  Created by Manuel on 15/01/2020.
//  Copyright © 2020 Manuel. All rights reserved.
//
import Foundation.NSDate

struct LogEntry {
    let location: String?
    let explanation:String
    let message: Message?
}

enum LogLevel {
    case error
    case info
    case full
    case none
}

func log(with logger: @escaping (String) -> (), level: LogLevel) -> (Message) -> () {
    
    logger("LogLevel: \(level)")
    
    return { msg in
        func logUnknownLevel(msg: Message) -> (){
           let noHigherLevelTriggered = [
                createLogEntryForFullLevel(msg: msg),
                createLogEntryForInfoLevel(msg: msg),
                createLogEntryForErrorLevel(msg: msg)
            ].allSatisfy {
                $0 == nil
            }
            
            if noHigherLevelTriggered {
                logLogEntry(LogEntry(location: nil, explanation: "uknown message", message: msg), "UNKNOWN", logger)
            }
        }
        
        func logErrorLevel(msg: Message) {
            if let logEntry = createLogEntryForErrorLevel(msg: msg) {
                logLogEntry(logEntry, "ERROR", logger)
            } else {
                logUnknownLevel(msg: msg)
            }
        }
        
        func logInfoLevel(msg: Message) {
            if let logEntry = createLogEntryForInfoLevel(msg: msg) {
                logLogEntry(logEntry, "INFO", logger)
            } else {
                logErrorLevel(msg: msg)
            }
        }
        
        func logFullLevel(msg: Message) {
            if let logEntry = createLogEntryForFullLevel(msg: msg) {
                logLogEntry(logEntry, "FULL", logger)
            } else {
                logInfoLevel(msg: msg)
            }
        }
        
        switch level {
        case .error: logErrorLevel(msg: msg)
        case  .info: logInfoLevel(msg: msg)
        case  .full: logFullLevel(msg: msg)
        case  .none: logUnknownLevel(msg: msg)
        }
    }
}

private
func logLogEntry(_ logEntry:LogEntry, _ prefix: String, _ logger: (String) -> ()) -> () {
    let date = Date()
    let dateString = "\(date) \("\(date.timeIntervalSince1970)".suffix(18).padding(toLength: 18, withPad: "0", startingAt: 0))"
    let locationString = "\(logEntry.location != nil ? "\(logEntry.location!)" : "unknown location")"
    let prefixString = "\(prefix.padding(toLength: 7, withPad: " ", startingAt: 0))"
    let explanationString = "\(logEntry.explanation == "" ? " " : " \(logEntry.explanation) ")"
    let msgString = logEntry.message != nil ? "\(logEntry.message!)" : "no message"
    logger("[\(dateString)] [\(prefixString)] <\(locationString)>:\(explanationString)<\(msgString)>")
}

private
func createLogEntryForErrorLevel(msg: Message) -> LogEntry? {
    if case .log(.error(let logEntry)) = msg {
        return logEntry
    }
    return nil
}

private
func createLogEntryForInfoLevel(msg: Message) -> LogEntry? {
    if case .log( .info(let logEntry)) = msg {
        return logEntry
    }
    return nil
}

private
func createLogEntryForFullLevel(msg: Message) -> LogEntry? {
    return LogEntry(location: nil, explanation: "", message: msg)
}
