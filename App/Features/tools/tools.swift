//
//  Feature.swift
//  Fiary
//
//  Created by Manuel on 09/01/2020.
//  Copyright © 2020 Manuel. All rights reserved.
//

func tools(_ msg: Message) -> Void {
    if case .feature(.tools(.print(let x))) = msg {
        print(x)
    }
}

protocol Logger {
    func log(_ string: String)
}

struct PrintLogger: Logger {
    func log(_ string: String)  {
        print("\(string)")
    }
}


