//
//  Feature.swift
//  Fiary
//
//  Created by Manuel on 09/01/2020.
//  Copyright © 2020 Manuel. All rights reserved.
//

func average(ofLast: Int, handler: @escaping (Message) -> ()) -> (Message) -> () {
    let size = ofLast
    var values: [Any] = []
    
    func eval() {
        if values.count == size {
            if let vals = values as? [Int] {
                handler(.feature(.random(.averageFromLastPicks(size, Double(vals.reduce(0, +)) / Double(size)))))
                values.remove(at: 0)
            }
        }
    }
    
    return { msg in
        if case .feature(.random(.pickedRandomly(let el))) = msg {
            values.append(el)
            eval()
        }
    }
}

func random(handler: @escaping (Message) -> ()) -> (Message) -> () {
    let rnd = Radomizer { response in
        switch response {
        case .elementPicked(let el):
            handler(.feature(.random(.pickedRandomly(el))))
        }
    }

    func sendRandomElement(lowerBound: Int, upperBound:Int) {
        rnd.handle(request: .pickRandomElement(Array(lowerBound..<upperBound))) // Possible Response: .pickedRandomly
    }
    
    func sendLowerBoundGreaterUpperBoundsError(msg: Message) -> () {
        handler(.feature(.random(.error(.inputError("upperbound needs to be greater than lowerbound")))))
        handler(.log(.error(LogEntry(location: #function, explanation: "upperbound needs to be greater than lowerbound", message: msg))))
    }
    
    func pick(lowerBound: Int, upperBound:Int, msg:Message) {
        lowerBound < upperBound
            ? sendRandomElement(lowerBound:lowerBound, upperBound: upperBound)
            : sendLowerBoundGreaterUpperBoundsError(msg: msg)
    }
    
    return { msg in
        if case .feature(.random(.range(let lb, let ub))) = msg {
            pick(lowerBound: lb, upperBound: ub, msg: msg)
        }
    }
}
