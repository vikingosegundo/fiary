//
//  Radomizer.swift
//  Fiary
//
//  Created by Manuel on 09/01/2020.
//  Copyright © 2020 Manuel. All rights reserved.
//

struct Radomizer: UseCase {
    
    enum Request {
        case pickRandomElement([Any])
    }
    
    enum Response {
        case elementPicked(Any)
    }
    
    typealias RequestType = Request
    typealias ResponseType = Response

    init(responseHandler: @escaping ((Radomizer.Response) -> ())) {
        self.reponseHandler = responseHandler
    }
    
    private let reponseHandler: (Response) -> ()
    
    func handle(request: Radomizer.Request) {
        switch request {
        case .pickRandomElement(let list):
            if let el = list.randomElement() {
                self.reponseHandler(.elementPicked(el))
            }
        }
    }
}
