//
//  Message.swift
//  LibExample
//
//  Created by Manuel on 08/10/2019.
//  Copyright © 2019 Harold. All rights reserved.
//

indirect
enum Message {
    
    case feature(Feature)
    case log(Log)
    case state
    
    enum Log {
        case error(LogEntry)
        case info(LogEntry)
    }
    
    enum Feature {
        case random(Random)
        case tools(Tools)
        
        enum Random {
            case range(Int, Int)
            case pickedRandomly(Any)
            case averageFromLastPicks(Int, Double)
            case error(RandomError)
            
            enum RandomError: Error {
                case inputError(String)
            }
        }
        
        enum Tools {
            case print(Any)
        }
    }
}
