//
//  MessageTabBarController.swift
//  LibExample
//
//  Created by Manuel on 16/10/2019.
//  Copyright © 2019 Harold. All rights reserved.
//

//import UIKit
//
//class MessageTabBarController: UITabBarController, MessageHandling {
//    
//    init(responseHandler: @escaping (Message) -> () ) {
//        self.responseHandler = responseHandler
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    
//    var responseHandler: (Message) -> () {
//        didSet { viewControllers?.compactMap { $0 as? ResponseHandling}.forEach {  $0.responseHandler = responseHandler } }
//    }
//    
//    func handle(msg: Message) {
//        viewControllers?.compactMap { $0 as? MessageHandling}.forEach { $0.handle(msg: msg)}
//    }
//}
