//
//  RandomNumberViewController.swift
//  Fiary
//
//  Created by Manuel on 08/01/2020.
//  Copyright © 2020 Manuel. All rights reserved.
//

import UIKit

class RandomNumberViewController: MessageViewController {
    
    @IBOutlet weak var lowerBoundField: UITextField!
    @IBOutlet weak var upperBoundField: UITextField!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var averageOfThree: UILabel!
    @IBOutlet weak var averageOfFive: UILabel!

    @IBAction func generateRandomNumberTapped(_ sender: Any) {
        
        let fields = [lowerBoundField!, upperBoundField!]
        let defaults = ["0", "100"]
        
        zip(fields, defaults).forEach { (textField, defaultValue) in
            if
                let text = textField.text,
                text == "" || Int(text) == nil
            { textField.text = defaultValue }
        }
        if
            let lowerBound = Int(lowerBoundField.text!),
            let upperBound = Int(upperBoundField.text!)
        {
            responseHandler?(.feature(.random(.range(lowerBound, upperBound + 1))))
        }
    }
    
    override func handle(msg: Message) {
        switch msg {
        case .feature(.random(.pickedRandomly(let x))): pickedRandomly(value: x)
        case .feature(.random(.error(.inputError(let errorText)))): errorOrcured(errorText: errorText)
        case .feature(.random(.averageFromLastPicks(let numberOfnumbers, let average))): self.averrage(for: numberOfnumbers, average: average)
        default:
            break
        }
    }
    
    private func pickedRandomly(value: Any) {
        numberLabel.text = "\(value)"
    }
    
    private func errorOrcured(errorText: String) {
        numberLabel.text = nil
        let alert = UIAlertController(title: "Error", message: errorText, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func averrage(for picks: Int, average: Double) {
        switch picks {
        case 3: averageOfThree.text = "\(average)"
        case 4: averageOfFive.text = "\(average)"
        default: break
        }
    }
    
}
