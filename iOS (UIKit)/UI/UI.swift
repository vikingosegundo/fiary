//
//  UI.swift
//  Fiary
//
//  Created by Manuel on 13/01/2020.
//  Copyright © 2020 Manuel. All rights reserved.
//

import UIKit

class UI {
    init(rootHandler: @escaping (Message) -> ()) {
        self.rootHandler = rootHandler
    }
    
    private let rootHandler: (Message) -> ()
    
    private
    func viewController() -> MessageViewController {
        let vc = MessageViewControllerFactory(kind: .randomNumber, responseHandler: rootHandler).make()
        self.vc = vc
        return vc
    }
    
    func window() -> UIWindow {
        let tabBarController = UITabBarController()
        
        tabBarController.setViewControllers([UINavigationController(rootViewController: viewController())], animated: false)
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        
        window.rootViewController = tabBarController
        window.makeKeyAndVisible()
        return window
    }
    
    func handle(msg: Message) {
        vc?.handle(msg: msg)
    }
    
    private var vc:MessageViewController?
}
