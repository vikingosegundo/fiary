//
//  MessageViewControllerFactory.swift
//  LibExample
//
//  Created by Manuel on 18/12/2019.
//  Copyright © 2019 Harold. All rights reserved.
//

import Foundation

enum FactoryKind {
    case randomNumber
}

protocol MessageViewControllerFactoring {
    func make() -> MessageViewController
}

final class MessageViewControllerFactory: MessageViewControllerFactoring {
    init(kind: FactoryKind, responseHandler: @escaping (Message) -> ()) {
        self.kind = kind
        self.responseHandler = responseHandler
    }
    
    private let kind: FactoryKind
    let responseHandler: (Message) -> ()
    
    func make() -> MessageViewController {
        
        switch kind {
        case .randomNumber:
            
            let vc = RandomNumberViewController()
            vc.responseHandler = responseHandler
            return vc
        }
    }
}
