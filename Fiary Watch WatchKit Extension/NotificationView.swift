//
//  NotificationView.swift
//  Fiary Watch WatchKit Extension
//
//  Created by Manuel on 02/02/2020.
//  Copyright © 2020 Manuel. All rights reserved.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
