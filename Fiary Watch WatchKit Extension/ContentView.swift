//
//  ContentView.swift
//  dareyou
//
//  Created by Manuel on 01/02/2020.
//  Copyright © 2020 Manuel. All rights reserved.
//

import Combine
import SwiftUI

struct ContentView: View {
    
    init(handler: @escaping (Message) -> ()) {
        self.handler = handler
    }
    
    @ObservedObject fileprivate var state = StateHolder()
    @State private var dieIsRolling = false
    
    private let handler: (Message) -> ()
    
    private func rollDie() {
        self.dieIsRolling = true
        self.state.random = nil
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
            self.handler(.feature(.random(.range(
                self.state.die.range.lowerBound,
                self.state.die.range.upperBound + 1)))
            )
            self.dieIsRolling = false
        }
    }
    
    fileprivate
    struct DieButtonStyle: ButtonStyle {
        let die: Die
        let state: StateHolder
        
        func makeBody(configuration: Self.Configuration) -> some View {
            configuration.label
                .padding()
                .frame(width: 48, alignment: .center)
                .foregroundColor(.white)
                .background(die == state.die ? Color.green : Color.gray)
                .cornerRadius(8.0)
                .font(.system(size: 12))
        }
    }
    
    var body: some View {
        VStack {
            HStack{
                Button(action: { self.state.die = dice[0] }) { Text(dice[0].title) }.buttonStyle(DieButtonStyle(die: dice[0], state: state))
                Button(action: { self.state.die = dice[1] }) { Text(dice[1].title) }.buttonStyle(DieButtonStyle(die: dice[1], state: state))
                Button(action: { self.state.die = dice[2] }) { Text(dice[2].title) }.buttonStyle(DieButtonStyle(die: dice[2], state: state))
            }
            Spacer()
            createDieView(die: state.die, value: state.random)
            Spacer()
            Button(action: rollDie) { Text(titleFor(die: state.die)) }.disabled(dieIsRolling)
        }
    }
    
    private func titleFor(die: Die) -> String { ["Roll", "Flip", "Shake"][dice.firstIndex(of: die) ?? 0] }
    
    func handle(msg: Message) {
        if
            case .feature(.random(.pickedRandomly(let x))) = msg,
            let xInt = x as? Int { state.random = xInt }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(handler: { _ in })
    }
}

fileprivate let cubeFaces = ["⚀", "⚁","⚂","⚃","⚄","⚅"]
fileprivate let coinFaces = [Image(systemName: "star.fill"), Image(systemName: "star")]

fileprivate
func createDieView(die: Die, value: Int?) -> some View {
    guard let value = value else { return AnyView(Text(" ").font(.largeTitle))}
    switch die.faceStyle {
    case .text(let p): return AnyView(p(value).font(.system(size: 20)).multilineTextAlignment(.center))
    case .cube(let p): return AnyView(p(value).font(.system(size: 60)))
    case .image(let p): return AnyView(p(value))
    }
}

fileprivate
let dice = [
    Die(
        title: "1-6",
        range: 1...6,
        faceStyle: .cube { Text(cubeFaces[$0 - 1]) }
    ),
    Die(
        title: "Coin",
        range: 0...1,
        faceStyle: .image{ coinFaces[$0] }
    ),
    Die(
        title: "8 Ball",
        range: 0...eightBallAnswers.count - 1,
        faceStyle: .text{ Text(eightBallAnswers[$0]) }
    )
]


fileprivate
let eightBallAnswers = [
    "It is certain.",
    "It is decidedly so.",
    "Without a doubt.",
    "Yes - definitely.",
    "You may rely on it.",
    "As I see it, yes.",
    "Most likely.",
    "Outlook good.",
    "Yes.",
    "Signs point to yes.",
    "Reply hazy, try again.",
    "Ask again later.",
    "Better not tell you now.",
    "Cannot predict now.",
    "Concentrate and ask again.",
    "Don't count on it.",
    "My reply is no.",
    "My sources say no.",
    "Outlook not so good.",
    "Very doubtful."
]

fileprivate
class StateHolder: ObservableObject {
    let objectWillChange = ObservableObjectPublisher()
    var random: Int? { willSet { objectWillChange.send() } }
    var die: Die = dice[1] { willSet { random = nil; objectWillChange.send() } }
}
