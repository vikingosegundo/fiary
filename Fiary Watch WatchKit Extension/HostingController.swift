//
//  HostingController.swift
//  Fiary Watch WatchKit Extension
//
//  Created by Manuel on 02/02/2020.
//  Copyright © 2020 Manuel. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<ContentView> {
    var app: ((Message) -> ())!
    var contentView: ContentView!
    
    override init() {
        super.init()
        contentView = ContentView{ self.app($0) }

        app = createApp(
            rootHandler: { self.app($0) },
            logLevel: .full,
            receivers: [{ self.body.handle(msg: $0) }]
        )
    }
    
    override var body: ContentView {
        return contentView
    }
}
