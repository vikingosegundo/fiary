//
//  Die.swift
//  Fiary TV
//
//  Created by Manuel Meyer on 01/03/2020.
//  Copyright © 2020 Manuel. All rights reserved.
//

import SwiftUI

enum FaceStyle {
    case text((Int) -> Text)
    case cube((Int) -> Text)
    case image((Int) -> Image)
}


struct Die: Hashable {
    let title: String
    let range: ClosedRange<Int>
    let faceStyle: FaceStyle
    

    func hash(into hasher: inout Hasher) {
        hasher.combine(title)
        hasher.combine(range)
    }
    
    static func == (lhs: Die, rhs: Die) -> Bool {
           lhs.range == rhs.range
        && lhs.title == rhs.title
    }
}

